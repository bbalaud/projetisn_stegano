from PIL import Image, ImageTk  
import  tkinter as Tk
from tkinter.filedialog import askopenfilename
import imageio
from d2b import d2b


root = Tk.Tk()
root.title('Stéganographie')

def retirer_couleurs(nom_f,couleur,l,h):
    image = imageio.imread(nom_f)
    for i in range(0,l):
        for j in range(0,h):
            (rouge,vert,bleu)=image[j,i]
            if couleur=="rouge" : image[j,i] = (rouge,0,0)
            if couleur=="vert" : image[j,i] = (0,vert,0)
            if couleur=="bleu" : image[j,i] = (0,0,bleu)
    imageio.imsave(couleur+".bmp", image)

def codage(l,h,etat):
    global fichier_stegano
    global nombre_bits

    try:
        nombre_bits=nombre_bits_menu
    except NameError:
        nombre_bits=1

    print("codage - nombre de bits :",nombre_bits)
    image = imageio.imread(fichier_original)
    if etat=="c" :
        image_code = imageio.imread(fichier_code)
        for i in range(0,l):
            for j in range(0,h):
                (rouge,vert,bleu)=image[j,i]
                (r_code,v_code,b_code)=image_code[j,i]
                rouge=rouge & (255-(pow(2,nombre_bits)-1))
                vert=vert & (255-(pow(2,nombre_bits)-1))
                bleu=bleu & (255-(pow(2,nombre_bits)-1))
                r_code=r_code//(pow(2,8-nombre_bits))
                v_code=v_code//(pow(2,8-nombre_bits))
                b_code=b_code//(pow(2,8-nombre_bits))
                image[j,i] = (rouge+r_code,vert+v_code,bleu+b_code)
    else :
        a=0
        b=0
        c=0
        for i in range(0,l):
            for j in range(0,h):
                (rouge,vert,bleu)=image[j,i]
                rouge=rouge & (255-(pow(2,nombre_bits)-1))
                vert=vert & (255-(pow(2,nombre_bits)-1))
                bleu=bleu & (255-(pow(2,nombre_bits)-1))
                for k in range(0,nombre_bits) :
                    bit0=(ord(donnees[c//8]) & pow(2,b))//pow(2,b)
                    rouge=rouge+bit0*pow(2,k)
                    a=(a+1)%24
                    b=(b+1)%8
                    c=c+1
                    bit1=(ord(donnees[c//8]) & pow(2,b))//pow(2,b)
                    vert=vert+bit1*pow(2,k)
                    a=(a+1)%24
                    b=(b+1)%8
                    c=c+1
                    bit2=(ord(donnees[c//8]) & pow(2,b))//pow(2,b)
                    bleu=bleu+bit2*pow(2,k)
                    a=(a+1)%24
                    b=(b+1)%8
                    c=c+1
                image[j,i] = (rouge,vert,bleu)
    separer=fichier_original.split('.')
    fichier_stegano=separer[0]+"_code_txt.bmp"
    imageio.imsave(fichier_stegano, image)
   

def decodage(l,h,etat):
    global fichier_decode
    global donnees_d
    global bit_bourrage
    
    try:
        bit_bourrage=bit_bourrage_menu
    except NameError:
        bit_bourrage=0

    print("décodage - nombre de bits :",nombre_bits)
    image = imageio.imread(fichier_stegano)
    if etat=="c" :
        print("bit de bourrage :",bit_bourrage)
        for i in range(0,l):
            for j in range(0,h):
                (rouge,vert,bleu)=image[j,i]
                rouge=(rouge%pow(2,nombre_bits))*(pow(2,8-nombre_bits))
                vert=(vert%pow(2,nombre_bits))*(pow(2,8-nombre_bits))
                bleu=(bleu%pow(2,nombre_bits))*(pow(2,8-nombre_bits))

                if bit_bourrage==1 :
                    rouge=rouge + pow(2,8-nombre_bits)-1
                    vert=vert + pow(2,8-nombre_bits)-1
                    bleu=bleu + pow(2,8-nombre_bits)-1
                
                image[j,i] = (rouge,vert,bleu)
        separer=fichier_original.split('.')
        fichier_decode=separer[0]+"_decode.bmp"
        imageio.imsave(fichier_decode, image)
    else :
        a=0
        b=0
        c=0
        data=0
        donnees_d=""
        for i in range(0,l):
            for j in range(0,h):
                (rouge,vert,bleu)=image[j,i]
                for k in range(0,nombre_bits):
                    bit0=(rouge & pow(2,k))//pow(2,k)
                    data=data//2+bit0*128
                    b=b+1
                    if b==8 :
                        b=0
                        donnees_d=donnees_d+chr(data)
                        data=0
                    bit1=(vert & pow(2,k))//pow(2,k)
                    data=data//2+bit1*128
                    b=b+1
                    if b==8 :
                        b=0
                        donnees_d=donnees_d+chr(data)
                        data=0
                    bit2=(bleu & pow(2,k))//pow(2,k)
                    data=data//2+bit2*128
                    b=b+1
                    if b==8 :
                        b=0
                        donnees_d=donnees_d+chr(data)
                        data=0
        separer=fichier_original.split('.')
        fichier_decode=separer[0]+"_decode.txt"
        f_texte=open(fichier_decode,"w")
        f_texte.write(donnees_d)
 
def Aide():
    print("")

def afficher_erreur(erreur) :
        f_erreur = Tk.Tk()
        f_erreur.title("Erreur")
        can_e = Tk.Canvas(f_erreur, width = 250, height = 50)
        can_e.create_text(100, 15, text=erreur)
        can_e.pack()

def analyse_original():
    global fichier_analyse
    print(bit_analyse)
    image = imageio.imread(fichier_original)
    for i in range(0,largeur):
        for j in range(0,hauteur):
            (rouge,vert,bleu)=image[j,i]
            rouge=(rouge & pow(2,bit_analyse)) * (pow(2,8-bit_analyse)-1)
            vert=(vert & pow(2,bit_analyse)) * (pow(2,8-bit_analyse)-1)
            bleu=(bleu & pow(2,bit_analyse)) * (pow(2,8-bit_analyse)-1)
            image[j,i] = (rouge,vert,bleu)
    separer=fichier_original.split('.')
    fichier_analyse=separer[0]+"_analyse_original.bmp"
    imageio.imsave(fichier_analyse, image)
    image_a = Image.open(fichier_analyse)
    photo = ImageTk.PhotoImage(image_a)
    canvas.create_image(largeur,50, anchor = Tk.NW, image=photo)
    canvas.create_text(largeur+100, 20, text="Analyse original")
    canvas.image_a = photo
    canvas.pack()
    bouton = Tk.Button(canvas, text="Analyse freq", fg="black", command=analyse_freq_o)
    bouton.place(x=largeur+largeur//4,y=hauteur+60)

def analyse_freq_o() :
    image = imageio.imread(fichier_analyse)
#    freq_rouge=0
#    freq_verte=0
#    freq_bleue=0
    freq=0
    for i in range(1,largeur-1):
        for j in range(1,hauteur-1):
            (rouge,vert,bleu)=image[j,i]
            rouge=rouge&pow(2,bit_analyse)
            vert=vert&pow(2,bit_analyse)
            bleu=bleu&pow(2,bit_analyse)
#            r8=0
#            v8=0
#            b8=0
            f=0
            for m in range(-1,2) :
                for n in range(-1,2):
                    (r,v,b)=image[j+n,i+m]
                    r=r&pow(2,bit_analyse)
                    v=v&pow(2,bit_analyse)
                    b=b&pow(2,bit_analyse)
                    if (r==rouge) and (v==vert) and (b==bleu): f=f+1
            freq=freq+f/9
#                    r8=r8+pow(int(rouge)-int(r),2)
#                    v8=v8+pow(int(vert)-int(r),2)
#                    b8=b8+pow(int(bleu)-int(r),2)
#            freq_rouge=freq_rouge+r8/8
#            freq_verte=freq_verte+v8/8
#            freq_bleue=freq_bleue+b8/8
#    freq_rouge=round(freq_rouge/(largeur*hauteur),3)
#    freq_verte=round(freq_verte/(largeur*hauteur),3)
#    freq_bleue=round(freq_bleue/(largeur*hauteur),3)
#    canvas.create_text(largeur+largeur//2, hauteur+100, text="rouge : "+str(freq_rouge)+" vert : "+str(freq_verte)+" bleu : "+str(freq_bleue))
    freq=round(freq/(largeur*hauteur),3)
    canvas.create_text(largeur+largeur//3, hauteur+100, text="Fréquence : "+str(freq))
    canvas.pack()    

def analyse_freq_c() :
    global label
    print(bit_analyse)
    image = imageio.imread(fichier_stegano)
    freq=0
    for i in range(1,largeur-1):
        for j in range(1,hauteur-1):
            (rouge,vert,bleu)=image[j,i]
            rouge=rouge&pow(2,bit_analyse)
            vert=vert&pow(2,bit_analyse)
            bleu=bleu&pow(2,bit_analyse)
#            r8=0
#            v8=0
#            b8=0
            f=0
            for m in range(-1,2) :
                for n in range(-1,2):
                    (r,v,b)=image[j+n,i+m]
                    r=r&pow(2,bit_analyse)
                    v=v&pow(2,bit_analyse)
                    b=b&pow(2,bit_analyse)
                    if (r==rouge) and (v==vert) and (b==bleu): f=f+1
            freq=freq+f/9
#                    r8=r8+pow(int(rouge)-int(r),2)
#                    v8=v8+pow(int(vert)-int(r),2)
#                    b8=b8+pow(int(bleu)-int(r),2)
#            freq_rouge=freq_rouge+r8/8
#            freq_verte=freq_verte+v8/8
#            freq_bleue=freq_bleue+b8/8
#    freq_rouge=round(freq_rouge/(largeur*hauteur),3)
#    freq_verte=round(freq_verte/(largeur*hauteur),3)
#    freq_bleue=round(freq_bleue/(largeur*hauteur),3)
#    canvas.create_text(largeur+largeur//2, hauteur+100, text="rouge : "+str(freq_rouge)+" vert : "+str(freq_verte)+" bleu : "+str(freq_bleue))
    freq=round(freq/(largeur*hauteur),3)
    try:
        canvas.delete(label) 
        label=canvas.create_text(largeur*4+largeur//3, hauteur+100, text="Fréquence : "+str(freq))
    except:
        label=canvas.create_text(largeur*4+largeur//3, hauteur+100, text="Fréquence : "+str(freq))
    canvas.pack() 


def fichier_original() :
    global fichier_original
    global canvas
    global largeur
    global hauteur
    global bouton
    global bit_analyse
    
    try:
        bit_analyse=bit_analyse_menu
    except NameError:
        bit_analyse=0

    chemin = askopenfilename(filetypes=[('BMP FILES','*.bmp')])
    separer=chemin.split('/')
    fichier_original = separer[len(separer)-1]
    try:
        image = Image.open(fichier_original)
    except:
        afficher_erreur("Erreur ouverture fichier")
        return
    
    photo = ImageTk.PhotoImage(image)
    largeur=image.size[0]
    hauteur=image.size[1]
    canvas = Tk.Canvas(root, width = image.size[0]*6, height = image.size[1]+120)
    canvas.create_image(0,50, anchor = Tk.NW, image=photo)
    canvas.create_text(100, 20, text="Original")
    canvas.image = photo
    canvas.pack()
    bouton = Tk.Button(canvas, text="Analyse Bit"+str(bit_analyse), fg="black", command=analyse_original)
    bouton.place(x=largeur//4,y=hauteur+60)

def clic_inactif() :
    print("toto")

def analyse_steganographie() :
    image = imageio.imread(fichier_stegano)
    for i in range(0,largeur):
        for j in range(0,hauteur):
            (rouge,vert,bleu)=image[j,i]
            rouge=(rouge & pow(2,bit_analyse)) * (pow(2,8-bit_analyse)-1)
            vert=(vert & pow(2,bit_analyse)) * (pow(2,8-bit_analyse)-1)
            bleu=(bleu & pow(2,bit_analyse)) * (pow(2,8-bit_analyse)-1)
            image[j,i] = (rouge,vert,bleu)
    separer=fichier_original.split('.')
    fichier_analyse=separer[0]+"_analyse_code.bmp"
    imageio.imsave(fichier_analyse, image)
    image_as = Image.open(fichier_analyse)
    photo = ImageTk.PhotoImage(image_as)
    canvas.create_image(largeur*4,50, anchor = Tk.NW, image=photo)
    canvas.create_text(largeur*4+100, 20, text="Analyse stéganographie")
    canvas.image_as = photo
    canvas.pack()
    bouton = Tk.Button(canvas, text="Analyse freq", fg="black", command=analyse_freq_c)
    bouton.place(x=largeur*4+largeur//4,y=hauteur+60)

def clic_décodage_image() :
        decodage(largeur,hauteur,"c")
        image_d = Image.open(fichier_decode)
        photo = ImageTk.PhotoImage(image_d)
        canvas.create_image(largeur*5,50, anchor = Tk.NW, image=photo)
        canvas.create_text(largeur*5+100, 20, text="Décodage")
        canvas.image_d = photo
        canvas.pack()
    
def clic_demarrer_code() :
        codage(largeur,hauteur,"c")
        image_s = Image.open(fichier_stegano)
        photo = ImageTk.PhotoImage(image_s)
        canvas.create_image(largeur*3,50, anchor = Tk.NW, image=photo)
        canvas.create_text(largeur*3+100, 20, text="Stéganographie")
        canvas.image_s = photo
        canvas.pack()
        bouton = Tk.Button(canvas, text="Analyse Bit"+str(bit_analyse), fg="black", command=analyse_steganographie)
        bouton.place(x=largeur*3+largeur*0.15,y=hauteur+60)
        bouton = Tk.Button(canvas, text="Décodage", fg="black", command=clic_décodage_image)
        bouton.place(x=largeur*3+largeur*0.65,y=hauteur+60)  


def clic_décodage_texte() :
        global texte1
        decodage(largeur,hauteur,"t")
        canvas.create_text(largeur*5+100, 20, text="Décodage")
        canvas.pack()        
        texte1 = Tk.Text(canvas, height=14, width=28)
        texte1.place(x=largeur*5,y=50)
        texte1.insert(Tk.END,donnees_d)
        
def clic_demarrer_texte() :
        codage(largeur,hauteur,"t")
        image_s = Image.open(fichier_stegano)
        photo = ImageTk.PhotoImage(image_s)
        canvas.create_image(largeur*3,50, anchor = Tk.NW, image=photo)
        canvas.create_text(largeur*3+100, 20, text="Stéganographie")
        canvas.image_s = photo
        canvas.pack()
        bouton = Tk.Button(canvas, text="Analyse Bit"+str(bit_analyse), fg="black", command=analyse_steganographie)
        bouton.place(x=largeur*3+largeur*0.15,y=hauteur+60)
        bouton = Tk.Button(canvas, text="Décodage", fg="black", command=clic_décodage_texte)
        bouton.place(x=largeur*3+largeur*0.65,y=hauteur+60)           

    
def fichier_code() :
    global fichier_code
    try:
        canvas
    except :
        afficher_erreur("Ouvrir le fichier original")
        return  

    try :
        texte1.destroy()
    except :
        print("pb")
    try :
            canvas.image_s=""
            canvas.image_d=""
    except :
        print("pb1")
    try :
        texte.destroy()
    except :
        print("pb2")        
    chemin = askopenfilename(filetypes=[('BMP FILES','*.bmp')])
    separer=chemin.split('/')
    fichier_code = separer[len(separer)-1]
    image_c = Image.open(fichier_code)
    photo = ImageTk.PhotoImage(image_c)
    canvas.create_image(largeur*2,50, anchor = Tk.NW, image=photo)
    canvas.create_text(largeur*2+100, 20, text="Code")
    canvas.image_c = photo
    canvas.pack()
    bouton = Tk.Button(canvas, text="Coder", fg="black", command=clic_demarrer_code)
    bouton.place(x=largeur*2+largeur//3,y=hauteur+60)

def fichier_texte() :
    global fichier_texte
    global donnees
    global texte
    canvas.image_s=""
    try :
        texte1.destroy()
    except :
        print("pb")
    try :
            canvas.image_s=""
            canvas.image_d=""
    except :
        print("pb1")
    chemin = askopenfilename(filetypes=[('TXT FILES','*.txt')])
    separer=chemin.split('/')
    fichier_texte = separer[len(separer)-1]
    f_texte = open(fichier_texte, "r")
    donnees = f_texte.read()
    f_texte.close()
    canvas.create_text(largeur*2+100, 20, text="Code")
    canvas.pack()    
    texte = Tk.Text(canvas, height=14, width=28)
    texte.place(x=largeur*2,y=50)
    texte.insert(Tk.END,donnees)
    bouton = Tk.Button(canvas, text="Coder", fg="black", command=clic_demarrer_texte)
    bouton.place(x=largeur*2+largeur//3,y=hauteur+60)

    
def preference() :
    global s_nb_bits
    global s_bourrage
    global s_bit_analyse
    global fenetre

    fenetre = Tk.Tk()
    fenetre.title('Préférences')
    can_p = Tk.Canvas(fenetre, width = 250, height = 75)
    can_p.create_text(50, 15, text="Nombre de bits : ")
    can_p.create_text(50, 40, text="Bits de bourrage : ")
    can_p.create_text(50, 65, text="Bit analysé : ")
    can_p.pack()

    s_nb_bits = Tk.Spinbox(can_p,values=(1,2,3,4,5,6,7), width=2 )
    try:
        if nombre_bits_menu!=-1 : nombre_bits=nombre_bits_menu 
    except :
        nombre_bits=1  
    for i in range(nombre_bits-1):
            s_nb_bits.invoke("buttonup")
    s_nb_bits.pack()
    s_nb_bits.place(x=100,y=5)
    
    s_bourrage = Tk.Spinbox(can_p,values=(0,1), width=2 )
    try:
        if bit_bourrage_menu!=-1 : bit_bourrage=bit_bourrage_menu 
    except :
        bit_bourrage=0
    if bit_bourrage==1 : s_bourrage.invoke("buttonup")    
    s_bourrage.pack()
    s_bourrage.place(x=100,y=30)

    s_bit_analyse = Tk.Spinbox(can_p,values=(0,1,2,3,4,5,6,7), width=2 )
    try:
        if bit_analyse_menu!=-1 : bit_analyse=bit_analyse_menu
    except :
        bit_analyse=0 
    for i in range(bit_analyse-1):
            s_bit_analyse.invoke("buttonup")
    s_bit_analyse.pack()
    s_bit_analyse.place(x=100,y=55)
    
    b = Tk.Button(can_p, text="OK", command=clic_ok)
    b.place(x=225,y=50)

def clic_ok():
    global nombre_bits_menu
    global bit_bourrage_menu
    global bit_analyse_menu
    nombre_bits_menu=int(s_nb_bits.get())
    bit_bourrage_menu=int(s_bourrage.get())
    bit_analyse_menu=int(s_bit_analyse.get())
    fenetre.destroy()

def quitter() :
    root.destroy()

menubar = Tk.Menu(root)

menu1 = Tk.Menu(menubar, tearoff=0)
menu1.add_command(label="Ouvrir original", command=fichier_original)
menu1.add_command(label="Ouvrir code image", command=fichier_code)
menu1.add_command(label="Ouvrir code texte", command=fichier_texte)
menu1.add_command(label="Quitter", command=quitter)
menubar.add_cascade(label="Fichier", menu=menu1)

menu2 = Tk.Menu(menubar, tearoff=0)
menu2.add_command(label="Préférences", command=preference)
menubar.add_cascade(label="Edition", menu=menu2)

root.config(menu=menubar)
  
root.mainloop()
